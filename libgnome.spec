%global _default_patch_fuzz 2
%global schemalist \\\
	desktop_gnome_accessibility_keyboard \\\
	desktop_gnome_accessibility_startup \\\
	desktop_gnome_applications_at_mobility \\\
	desktop_gnome_applications_at_visual \\\
	desktop_gnome_applications_browser \\\
	desktop_gnome_applications_office \\\
	desktop_gnome_applications_terminal \\\
	desktop_gnome_applications_window_manager \\\
	desktop_gnome_background \\\
	desktop_gnome_file_views \\\
	desktop_gnome_interface \\\
	desktop_gnome_lockdown \\\
	desktop_gnome_peripherals_keyboard \\\
	desktop_gnome_peripherals_monitor \\\
	desktop_gnome_peripherals_mouse \\\
	desktop_gnome_sound \\\
	desktop_gnome_thumbnail_cache \\\
	desktop_gnome_thumbnailers \\\
	desktop_gnome_typing_break

Name:            libgnome
Version:         2.32.1
Release:         21
Summary:         Gnome 2.x desktop base library
URL:             http://www.gnome.org
Source0:         http://download.gnome.org/sources/libgnome/2.32/%{name}-%{version}.tar.bz2
Source1:         desktop_gnome_peripherals_monitor.schemas
License:         LGPLv2+

Requires:        utempter GConf2

BuildRequires:   zlib-devel glib2-devel libbonobo-devel GConf2-devel gnome-vfs2-devel
BuildRequires:   libxml2-devel ORBit2-devel libxslt-devel libcanberra-devel intltool
BuildRequires:   gnome-common autoconf automake libtool gettext popt-devel

Patch1:          libgnome-2.11.1-scoreloc.patch
Patch2:          im-setting.patch
Patch3:          0001-Don-t-use-G_DISABLE_DEPRECATED.patch

%description
This package contains the basic libraries for the GNOME 2.x Desktop
platform and it includes non-GUI-related libraries that are needed to run GNOME.

%package devel
Summary: Provides files and libraries for development
Requires: %{name} = %{version}-%{release}

%description devel
This package contains all necessary include files and libraries when
develop applications.

%package help
Summary: Provides help for libgnome

%description help
This package contains all help for libgnome.

%prep
%autosetup -p1

%build
%configure --disable-gtk-doc --disable-esd
%make_build

pushd po
grep -v ".*[.]desktop[.]in[.]in$\|.*[.]server[.]in[.]in$" POTFILES.in > POTFILES.keep
mv POTFILES.keep POTFILES.in
intltool-update --pot
sed -ie 's|POT-Creation-Date.*|POT-Creation-Date: 2008-10-01 00:00-0400\\n"|g' %{name}-2.0.pot
for p in *.po; do
  msgmerge $p %{name}-2.0.pot > $p.out
  msgfmt -o `basename $p .po`.gmo $p.out
done
popd

%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
%make_install
cp -p %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/gconf/schemas/

%delete_la_and_a

for serverfile in $RPM_BUILD_ROOT%{_libdir}/bonobo/servers/*.server; do
    sed -i -e 's|location *= *"/usr/lib\(64\)*/|location="/usr/$LIB/|' $serverfile
done

rm -rf $RPM_BUILD_ROOT%{_datadir}/gnome-background-properties $RPM_BUILD_ROOT%{_datadir}/pixmaps
mv COPYING.LIB COPYING 

%find_lang %{name}-2.0

%post
/sbin/ldconfig
%gconf_schema_upgrade %{schemalist}

%pre
%gconf_schema_prepare %{schemalist}

%preun
%gconf_schema_remove %{schemalist}

%postun -p /sbin/ldconfig

%files -f %{name}-2.0.lang
%doc COPYING
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_libdir}/bonobo/monikers/*
%{_libdir}/bonobo/servers/*
%{_sysconfdir}/gconf/schemas/*.schemas
%{_sysconfdir}/sound

%files devel
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

%files help
%doc AUTHORS NEWS README
%{_mandir}/man7/*
%{_datadir}/gtk-doc

%changelog
* Mon Jan 10 2022 liuyumeng <liuyumeng5@huawei.com> - 2.32.1-21
- delete autoreconf in spec

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.32.1-20
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.32.1-19
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec 

* Sat Sep 14 2019 liyongqiang<liyongqiang10@huawei.com> - 2.32.1-18
- Package init
